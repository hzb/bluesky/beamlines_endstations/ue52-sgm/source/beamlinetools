
# Define here data export callbacks
from datetime import datetime

from beamlinetools.callbacks.file_exporter import CSVCallback, SpecWriterCallback


from .base import RE

year = str(datetime.now().year)

specwriter = SpecWriterCallback(filename="/opt/bluesky/data/UE52SGM_spec_"+year+".spec")
RE.subscribe(specwriter.receiver)

# data manager, export scans to single csv file

dm = CSVCallback(file_path="/opt/bluesky/data")
dm.change_user("beamline_commissioning_"+year)
changeuser=dm.change_user
RE.subscribe(dm.receiver)