from bluesky.log import config_bluesky_logging
config_bluesky_logging()

import logging
import requests

proxies = {
  "http": "",
  "https": "",
}


class HTTPLogHandler(logging.Handler):
    def __init__(self, flask_log_url):
        super().__init__()
        self.flask_log_url = flask_log_url

    def emit(self, record):
        log_entry = self.format(record)
        payload = {
            'level': record.levelname,
            'message': log_entry
        }
        try:
            requests.post(self.flask_log_url, json=payload, proxies=proxies)
        except Exception as e:
            print(f"Error sending log to Flask: {e}")

# Flask log endpoint
FLASK_LOG_URL = 'http://0.0.0.0:5000/log'  # Replace with actual Flask container IP

# Configure Bluesky logging
logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger('bluesky')
http_handler = HTTPLogHandler(FLASK_LOG_URL)
http_handler.setLevel(logging.WARNING)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
http_handler.setFormatter(formatter)
logger.addHandler(http_handler)
