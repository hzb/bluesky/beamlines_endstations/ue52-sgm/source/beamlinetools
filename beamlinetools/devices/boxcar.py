from ophyd import EpicsSignal, EpicsSignalRO, Device
from ophyd import Component as Cpt
from ophyd import FormattedComponent as FCpt

from ophyd.utils import set_and_wait
from ophyd.status import SubscriptionStatus




class Boxcar(Device):


    bc1      = Cpt(EpicsSignalRO, "BC1", name='bc1', kind='hinted')
    bc2      = Cpt(EpicsSignalRO, "BC2", name='bc2', kind='hinted')
    ebc1     = Cpt(EpicsSignalRO, "EBC1", name='ebc1')
    ebc2     = Cpt(EpicsSignalRO, "EBC2", name='ebc2')
    nsam1    = Cpt(EpicsSignalRO, "NSam1", name='nsam1')
    nsam2    = Cpt(EpicsSignalRO, "NSam2", name='nsam2')
    trig     = Cpt(EpicsSignal, "Trig", name='Trig')
    int_time = Cpt(EpicsSignal, "IntTime", name='int_time')
    status   = Cpt(EpicsSignalRO, "Trig", name='status')

    def trigger(self, *args, **kwargs):
        def check_value(*, old_value, value, **kwargs):
            "Return True when the acquisition is complete, False otherwise."
            return (old_value==1 and value == 0  )
        status = SubscriptionStatus(self.status, check_value)
        self.trig.set(1)
        return status

