from ophyd import Component as Cpt
from ophyd import EpicsMotor,Device


class Sample(Device):
    x = Cpt(EpicsMotor, "X:m1", name='x')
    y = Cpt(EpicsMotor, "Y:m1", name='y')
    z = Cpt(EpicsMotor, "Z:m1", name='z')
